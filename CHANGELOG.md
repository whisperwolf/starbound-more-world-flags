# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Add
- Finish making refs & flags from [`checklist.txt`](checklist.txt)

### Change
- See if there's a way to make flags face right (not flipped) in the crafting UI

### Fix
- Do something about item names being so long they clip out of the UI
- Figure out if there's any way to avoid the crash caused by including the accents in "Côte d'Ivoire", "São Tomé and Príncipe", etc

## [0.4.2] - 2020-04-11
### Added
- <details>
  <summary>The following flags: (19 items)</summary>
  
  - Afghan Northern Alliance
  - Åland
  - Bornholm
  - Bornholm (alt)
  - Carnatic
  - Naval ensign of Denmark
  - Naval ensign of Iceland
  - Jutland
  - Maratha Empire
  - Mecklenburg
  - Mecklenburg-Vorpommern
  - Naval ensign of Norway
  - Pomerania
  - the Rhine
  - Rwanda (1959)
  - Saxony
  - South Africa
  - Naval ensign of Sweden
  - Vendsyssel
  </details>

## [0.4.1] - 2020-04-06
### Added
- <details>
  <summary>The following flags: (65 items)</summary>
  
  - Agender pride flag
  - Aromantic pride flag
  - Artsakh
  - Aruba
  - Asexual pride flag
  - Atlantium
  - Austenasia
  - Austrian Empire
  - Bisexual pride flag
  - Republic of China (1912)
  - Freetown Christiania
  - Cuba
  - Czech Republic (tricolour)
  - Djibouti
  - Elgaland-Vargaland
  - England
  - Faroe Islands
  - Free France
  - Genderfluid pride flag
  - Genderqueer pride flag
  - German Empire
  - Nazi Germany
  - Nazi Germany (alt)
  - Greece (alt)
  - Guernsey
  - Icaria
  - Intersex pride flag
  - Kingdom of Iraq
  - Kekistan
  - Ladonia
  - Lesbian pride flag
  - Malta
  - Manchukuo
  - Mengjiang
  - Molossia
  - Murrawarri Republic
  - Nauru
  - Nepal
  - Nonbinary pride flag
  - Pan-African flag
  - Panama
  - Pansexual pride flag
  - Peru
  - Philippines
  - Polysexual pride flag
  - Puerto Rico
  - Red Cross
  - Redonda
  - Rhenish Republic
  - Rwanda
  - Serbia
  - Flag of surrender
  - Switzerland (square)
  - Texas
  - Trans pride flag
  - Trinidad and Tobago
  - Republic of Vietnam
  - São Tomé and Príncipe
  - Socialist Republic of Vietnam
  - Xinjiang Clique
  - Yemen (modern)
  - Yemen Arab Republic (North Yemen)
  - South Yemen
  - Yugoslavia
  - Yugoslavia (socialist)
  </details>

## [0.4.0] - 2020-04-05
### Added
- Flags are now all in their own tabs sorted by continent! (Except Oceania, that got squashed into Misc)
- Added functionality to [`recipebuilder.bat`](templates/recipebuilder.bat) to choose the associated continent/category of a flag
- Flag of Croatia
- Flag of North Korea
- Flag of Uganda (modern)
- Flag of Uganda (1962)

### Changed
- Demonym for South Korea changed from "Korean" to "South Korean"

### Fixed
- A typo in the UI ("Crafing fancy flags!")

## [0.3.0] - 2020-04-04
### Added
- [Uploaded to the Steam Workshop!](https://steamcommunity.com/sharedfiles/filedetails/?id=2047628891)
- This means More World Flags is now officially in Beta!

### Changed
- Much prettier icon (using [Megrim](https://fonts.google.com/specimen/Megrim))
- Updated description field in [`_metadata`](moreworldflagssubmod/_metadata) to an abridged version of [`README.md`](README.md)

## [0.2.3] - 2020-04-03
### Added
- List of flags added to [`README.md`](README.md)
- <details>
  <summary>The following flags: (52 items)</summary>
  
  - Cambodia (modern)
  - Democratic Kampuchea (Communist Cambodia)
  - Cameroon
  - Cabo Verde
  - Central African Republic
  - Chad
  - Republic of China
  - Colombia
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Estonia (nordic)
  - Gabon
  - Gambia
  - Georgia
  - Ghana
  - Greece
  - Guangxi Clique
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Iceland
  - Jordan
  - Kuwait
  - Laos
  - Latvia
  - LGBT pride flag
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Lithuania
  - Luxembourg
  - Madagascar
  - Mali
  - Mauritius
  - Monaco
  - Morocco
  - Sultanate of Muscat and Oman
  - Myanmar
  - Niger
  - Nigeria
  - Palau
  - Palestine
  - Qatar
  - Senegal
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - Sudan
  - Suriname
  - Xibei San Ma
  - Yunnan Clique
  </details>

### Changed
- Demonym for Bosnia and Herzegovina changed from "Bosnian and Herzegovinian" to simply "Bosnian"
- "Bahamas, Commonwealth of" changed to "Bahamas, Commonwealth of the"
- "Shanxi" changed to "Shanxi Clique"; "Yunnan" changed to "Yunnan Clique"

### Fixed
- Ref for Palestine was previously undocumented in [v0.1.0](#010-2020-03-31)

## [0.2.2] - 2020-04-01
### Added
- <details>
  <summary>The following flags: (19 items)</summary>

  - Angola
  - Antigua and Barbuda
  - Armenia
  - Azerbaijan
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Belize
  - Benin
  - Bhutan
  - Bolivia
  - Bosnia and Herzegovina (modern)
  - Republic of Bosnia and Herzegovina
  - Socialist Republic of Bosnia and Herzegovina
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
</details>

### Changed
- flagbuilder.bat now references the input demonym instead of using generic examples during the article input (this is only a UI change)
- Pretty new icon with all sorts of different flags instead of only 4

## [0.2.1] - 2020-03-31
### Added
- Made a tutorial for ref and flag creation; that's in the [`README.md`](README.md) now
- Flag of Afghanistan
- Flag of Albania
- Flag of Algeria
- Flag of Andorra

### Changed
- <details>
  <summary>Updated localisation (ingame names) for the following flags: (21 items)</summary>

  - Argentina
  - Austria
  - Belgium
  - Brazil
  - Bulgaria
  - Chile
  - Denmark
  - Estonia
  - Finland
  - Indonesia
  - Ireland
  - Israel
  - North Macedonia
  - Mexico
  - Norway
  - Poland
  - Sweden
  - Switzerland
  - Thailand
  - Turkey
  - Uruguay
</details>

## [0.2.0] - 2020-03-31
### Added
- [Batch tool](templates/objectbuilder.bat) to automatically generate flag.object and flag.frames files
- [Another batch tool](templates/recipebuilder.bat) to automatically generate flag.recipe files

### Fixed
- Minor localisation fixes

## [0.1.0] - 2020-03-31
### Added
- Templates to help create more refs & sprites
- Localisation updates for all of the pre-existing flags ("Flag of X" instead of "X Flag" with full country names)
- Flag of the Bahamas
- <details>
  <summary>Refs for the following flags: (75 items)</summary>

  - Afghanistan
  - Albania
  - Algeria
  - Andorra
  - Angola
  - Antigua and Barbuda
  - Armenia
  - Azerbaijan
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Belize
  - Benin
  - Bhutan
  - Bolivia
  - Bosnia and Herzegovina (modern)
  - Bosnia and Herzegovina (republic)
  - Bosnia and Herzegovina (socialist)
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
  - Cambodia (modern)
  - Cambodia (communist)
  - Cameroon
  - Cabo Verde
  - Central African Republic
  - Chad
  - Republic of China
  - Colombia
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Estonia (nordic)
  - Gabon
  - Gambia
  - Georgia
  - Ghana
  - Greece
  - Guangxi Clique
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Iceland
  - Jordan
  - Kuwait
  - Laos
  - Latvia
  - Lithuania
  - LGBT pride flag (it's not a country, but it's well-known enough)
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Luxembourg
  - Madagascar
  - Mali
  - Mauritius
  - Monaco
  - Morocco
  - Sultanate of Muscat and Oman
  - Myanmar
  - Niger
  - Nigeria
  - Palau
  - Palestine
  - Qatar
  - Senegal
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - Sudan
  - Suriname
  - Xibei San Ma
  - Yunnan Clique
</details>