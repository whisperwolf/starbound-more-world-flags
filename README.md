# Starbound More World Flags submod

WIP submod for [Combat Blob's World Flags](https://steamcommunity.com/sharedfiles/filedetails/?id=729433195) to, hopefully in time, add 450 or so new flags.

Current version: [**0.4.2**](CHANGELOG.md#042-2020-04-11)

[`moreworldflagssubmod/`](moreworldflagssubmod) is the **MAIN MOD FOLDER.**  
&nbsp;&nbsp;&nbsp;&nbsp;This is what goes into `C:\Program Files (x86)\Steam\steamapps\common\Starbound\mods`. **That's all you need for installation.**

[`refs/`](refs) is a folder full of pdns containing flag refs, which will eventually be turned into ingame flags. This is done with [Flag Rippler for Paint.NET](../../../../pdn-flag-rippler).  
&nbsp;&nbsp;&nbsp;&nbsp;Refs which have already been turned into sprites are in [`refs/archived/`](refs/archived).  
&nbsp;&nbsp;&nbsp;&nbsp;Note that some are marked with "! large thumb ". These use a 9x7 instead of 9x6 px thumbnail.  
[`templates/`](templates) is a few .pngs used to help create refs and turn them into sprites.  
&nbsp;&nbsp;&nbsp;&nbsp;[`templates/objectbuilder.bat`](templates/objectbuilder.bat) creates `flag.object` and `flag.frames` files for you!  
&nbsp;&nbsp;&nbsp;&nbsp;[`templates/recipebuilder.bat`](templates/recipebuilder.bat) will create the `flag.recipe` file as well.  
[`checklist.txt`](checklist.txt) is a gigantic list of all the flags I'd like to include in this submod. It's not exactly glamorous, so I have a prettier list just of every flag that has been fully implemented right here:

<details>
  <summary><b>List of flags</b> (164 items)</summary>
  
  - Afghanistan
  - Afghan Northern Alliance
  - Agender pride flag
  - Åland
  - Albania
  - Algeria
  - Andorra
  - Angola
  - Antigua and Barbuda
  - Aromantic pride flag
  - Armenia
  - Artsakh
  - Aruba
  - Asexual pride flag
  - Atlantium
  - Austenasia
  - Austrian Empire
  - Azerbaijan
  - Bahamas
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Belize
  - Benin
  - Bhutan
  - Bisexual pride flag
  - Bolivia
  - Bornholm
  - Bornholm (alt)
  - Bosnia and Herzegovina (modern)
  - Republic of Bosnia and Herzegovina
  - Socialist Republic of Bosnia and Herzegovina
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
  - Cambodia (modern)
  - Democratic Kampuchea (Communist Cambodia)
  - Cameroon
  - Cabo Verde
  - Carnatic
  - Central African Republic
  - Chad
  - Republic of China (modern)
  - Republic of China (1912)
  - Freetown Christiania
  - Colombia
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Croatia
  - Cuba
  - Czech Republic (tricolour)
  - Naval ensign of Denmark
  - Djibouti
  - Elgaland-Vargaland
  - England
  - Estonia (nordic)
  - Faroe Islands
  - Free France
  - Gabon
  - Gambia
  - Genderfluid pride flag
  - Genderqueer pride flag
  - Georgia
  - German Empire
  - Nazi Germany
  - Nazi Germany (alt)
  - Ghana
  - Greece
  - Greece (alt)
  - Guangxi Clique
  - Guernsey
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Icaria
  - Iceland
  - Naval ensign of Iceland
  - Intersex pride flag
  - Kingdom of Iraq
  - Jordan
  - Jutland
  - North Korea
  - Kekistan
  - Kuwait
  - Ladonia
  - Laos
  - Latvia
  - Lesbian pride flag
  - LGBT pride flag
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Lithuania
  - Luxembourg
  - Madagascar
  - Mali
  - Malta
  - Manchukuo
  - Maratha Empire
  - Mauritius
  - Mecklenburg
  - Mecklenburg-Vorpommern
  - Mengjiang
  - Molossia
  - Monaco
  - Morocco
  - Murrawarri Republic
  - Sultanate of Muscat and Oman
  - Myanmar
  - Nauru
  - Nepal
  - Niger
  - Nigeria
  - Nonbinary pride flag
  - Naval ensign of Norway
  - Palau
  - Palestine
  - Pan-African flag
  - Panama
  - Pansexual pride flag
  - Peru
  - Philippines
  - Polysexual pride flag
  - Pomerania
  - Puerto Rico
  - Qatar
  - Red Cross
  - Redonda
  - Rhenish Republic
  - Rhine
  - Rwanda (modern)
  - Rwanda (1959)
  - São Tomé and Príncipe
  - Saxony
  - Senegal
  - Serbia
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - South Africa
  - Sudan
  - Suriname
  - Flag of surrender
  - Naval ensign of Sweden
  - Switzerland (square)
  - Texas
  - Trans pride flag
  - Trinidad and Tobago
  - Uganda (modern)
  - Uganda (1962)
  - Vendsyssel
  - Republic of Vietnam
  - Socialist Republic of Vietnam
  - Xibei San Ma
  - Xinjiang Clique
  - Yemen (modern)
  - Yemen Arab Republic (North Yemen)
  - South Yemen
  - Yugoslavia
  - Yugoslavia (socialist)
  - Yunnan Clique
</details>

[**Link to the Steam Workshop**](https://steamcommunity.com/sharedfiles/filedetails/?id=2047628891)  
[Link to Steam discussion page](https://steamcommunity.com/workshop/filedetails/discussion/729433195/2139714324745613701/)

### How to create a ref

1. Use [`templates/ref.png`](templates/ref.png) or [`templates/large ref.png`](templates/large%20ref.png) and draw the flag and its thumbnail inside the black outlines
2. Hide the layer with the black outlines and place the .pdn or .psd in [`refs/`](refs)
    - Precede its filename with a "! large thumb " if you used the large ref when creating it.

### How to create a flag

IMPORTANT: None of this entails modifying [`templates/`](templates); don't change that
1. Create a directory [`moreworldflagssubmod/objects/generic/newflag[country]/`](moreworldflagssubmod/objects/generic)
2. Use [`templates/sprite.png`](templates/sprite.png) and [Flag Rippler for Paint.NET](../../../../pdn-flag-rippler) to create a flag sprite; name this newflag[country].png and save to the flag directory
3. Use [`templates/thumb.png`](templates/thumb.png) or [`templates/large thumb.png`](templates/large%20thumb.png) to create a thumbnail; name this newflag[country]icon.png and save to the flag directory
4. Move the ref you used to [`refs/archived/`](refs/archived)
5. Copy [`templates/objectbuilder.bat`](templates/objectbuilder.bat) to the flag directory, and run it to generate the .object and .frames files. Delete the new .bat afterward.
6. Copy [`templates/recipebuilder.bat`](templates/recipebuilder.bat) to [`moreworldflagssubmod/recipes/emptyhands/`](moreworldflagssubmod/recipes/emptyhands) and run it to generate the .recipe file. Delete the new .bat afterward.
    - For the purposes of categorising, consider Central America and the West Indies to be part of North America.
    - All Oceanic countries go in misc. There wasn't enough space for a seventh tab without making the UI really cramped, so this is a sacrifice that has to be made.
7. Finally, edit [`moreworldflagssubmod/player.config.patch`](moreworldflagssubmod/player.config.patch) and add a line just like the others with the name of your flag. Please keep this in alphabetical order.
    - Make sure that every line except the final one ends in a comma; failing to do so can cause a crash on startup.

*Why prefix with "newflag-" instead of "flag-"?*  
This is done for two reasons:  
&nbsp;&nbsp;&nbsp;&nbsp;a. to differentiate the flags in the original mod with the flags in the submod  
&nbsp;&nbsp;&nbsp;&nbsp;b. Just in case development restarts on the original mod, this minimises the likelihood of any conflicts.