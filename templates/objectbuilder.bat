@echo off

REM get user input
set /p object="Object name (e.g. newflagbahamas): "
set /p demonym="Demonym (e.g. Bahamian): "
set /p full="Full country name (e.g. Bahamas,\nCommonwealth of): "
set /p article="Use A or An? (A/An %demonym% flag) (input A or An): "

REM build flag.object
(
  echo;{
  REM this is changed
  echo;  "objectName" : "%object%",
  echo;  "colonyTags" : ["human"],
  echo;  "printable" : false,
  echo;  "rarity" : "Common",
  echo;  "objectType" : "teleporter",
  echo;  "category" : "teleportMarker",
  echo;  "price" : 70,
  echo;  "radioMessagesOnPickup" : [ "pickupflag" ],
  echo;  
  REM this is changed
  echo;  "description" : "The %demonym% flag. Use ^green;[E] to bookmark a teleportation location.",
  REM also this
  echo;  "shortdescription" : "Flag of %full%",
  echo;  "race" : "none",
  echo;
  REM this block is changed
  echo;  "apexDescription" : "%article% %demonym% flag. I can use this as a waypoint for teleportation.",
  echo;  "avianDescription" : "This %demonym% flag can be bookmarked as a destination for my ship teleporter.",
  echo;  "floranDescription" : "%article% %demonym% flag makess good point to teleport back to ussing teleporter!",
  echo;  "glitchDescription" : "Observant. A small locator module attached to this %demonym% flag allows it to be bookmarked for teleportation.",
  echo;  "humanDescription" : "This %demonym% flag can be saved as a location for my teleporter. Then I can return to it at any time.",
  echo;  "hylotlDescription" : "This %demonym% flag can act as a way point, enabling me to return to this point using a teleporter.",
  echo;  "novakidDescription" : "I can bookmark this %demonym% flag for quick teleportin'.",
  echo;
  echo;  "interactAction" : "OpenTeleportDialog",
  echo;  "interactData" : "/interface/warping/destinationonly.config",
  echo;
  REM this is changed
  echo;  "inventoryIcon" : "%object%icon.png",
  echo;  "orientations" : [
  echo;    {
  REM this too
  echo;      "image" : "%object%.png:<color>.<frame>",
  echo;      "flipImages" : true,
  echo;      "direction" : "left",
  echo;      "imagePosition" : [-17, 0],
  echo;      "frames" : 6,
  echo;      "animationCycle" : 1.0,
  echo;
  echo;      "spaceScan" : 0.1,
  echo;      "anchors" : [ "bottom" ]
  echo;    },
  echo;    {
  REM and this
  echo;      "image" : "%object%.png:<color>.<frame>",
  echo;      "direction" : "right",
  echo;      "imagePosition" : [1, 0],
  echo;      "frames" : 6,
  echo;      "animationCycle" : 1.0,
  echo;
  echo;      "spaceScan" : 0.1,
  echo;      "anchors" : [ "bottom" ]
  echo;    }
  echo;  ]
  echo;
  echo;  //"soundEffect" : "/sfx/objects/flag_flap_loop.ogg"
  echo;}
) > "%object%.object"

REM build flag.frames (this doesn't require user input)
(
  echo;{
  echo;
  echo;  "frameGrid" : {
  echo;    "size" : [24, 32],
  echo;    "dimensions" : [6, 1],
  echo;    "names" : [
  echo;      [ "default.0", "default.1", "default.2", "default.3", "default.4", "default.5" ]
  echo;    ]
  echo;  },
  echo;
  echo;  "aliases" : {
  echo;    "default.default" : "default.0"
  echo;  }
  echo;}
) > "%object%.frames"