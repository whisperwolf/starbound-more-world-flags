@echo off

REM get user input
set /p object="Object name (e.g. newflagbahamas): "
set /p category="Continent name (europe, asia, africa, na, sa, misc): "

REM build flag.recipe
(
  echo;{
  echo;  "input" : [
  echo;    { "item" : "fabric", "count" : 10 }
  echo;  ],
  echo;  "output" : {
  REM this is changed
  echo;    "item" : "%object%",
  echo;    "count" : 1
  echo;  },
  echo;  "groups" : [ "flagstation", "flagstation%category%" ]
  echo;}
) > "%object%.recipe"